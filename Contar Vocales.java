import java.util.Scanner;

public class RetoVocales {

    public static void main(String[] args) {

        int vocalA = 0, vocalE = 0, vocalI = 0, vocalO = 0, vocalU = 0;

        Scanner teclado = new Scanner(System.in);

        System.out.println(“Introduce un texto: “);
        String texto = teclado.nextLine();
        for (int i = 0; i < texto.length(); i++) {
            switch (Character.toUpperCase(texto.charAt(i))) {
            case ‘A’:
                vocalA++;
                break;
            case ‘E’:
                vocalE++;
                break;
            case ‘I’:
                vocalI++;
                break;
            case ‘O’:
                vocalO++;
                break;
            case ‘U’:
                vocalU++;
                break;
            default:
                break;
            }
        }

        System.out.println(“La vocal A aparece: ” + vocalA) + "veces";
        System.out.println(“La vocal E aparece: ” + vocalE) + "veces";
        System.out.println(“La vocal I aparece: ” + vocalI) + "veces";
        System.out.println(“La vocal O aparece: ” + vocalO) + "veces";
        System.out.println(“La vocal U aparece: ” + vocalU) + "veces";

    }

}